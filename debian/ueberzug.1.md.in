% UEBERZUG(1) __VERSION__ | User Commands
% 
% November 2024

# NAME

**ueberzug** - command line utility which draws images on terminals

# SYNOPSIS

| **ueberzug** **layer** \[_OPTIONS_]

| **ueberzug** **library**

| **ueberzug** **version**

# DESCRIPTION

**ueberzug** is a command line utility which can draw images on terminals using child windows. It possesses many advantages over w3mimgdisplay, including: the use of chars as the size unit for position/size, tmux support, processing of expose events, and support for terminals that do not support WINDOWID.

**ueberzug** takes the following arguments:

    layer [OPTIONS]
        Display image

    library
        Print the path to the bash library

    version
        Print version information and exit

# OPTIONS

The _layer_ argument supports the following options:

-p, \-\-parser \[ _json_ | _simple_ | _bash_ ]

    json: JSON object per line (default)

    simple: Key-values separated by a tab character

    bash: associative array dumped via `declare -p`

-l, \-\-loader \[ _thread_ | _process_ | _synchronous_ ]

    thread: load image in thread (default)

    process: load image in additional process

    synchronous: load image right away

-s, \-\-silent

    Print STDERR to /dev/null

# COPYRIGHT

Copyright (c) 2018-2023 Ueberzug Developers

# LICENSE

Ueberzug is licensed under the GNU General Public License v3.0

# AUTHOR

This manual page is based on the ueberzug help documentation. It was created by Nick Morrott <nickm@debian.org> for the Debian GNU/Linux system, but may be used by others

# SEE ALSO

| **w3mimgdisplay**(1)

| Example scripts using ueberzug in /usr/share/doc/ueberzug/examples
